from django.forms import ModelForm, TextInput, Textarea

from django import forms

from washoku.models import Comment, Reply, Restaurant, Menu, Course

from cloudinary.forms import CloudinaryFileField #追加


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ('author', 'text')
        widgets = {
            'author': TextInput(attrs={
                'class': 'form-control',
                'placeholder': '名前',
            }),
            'text': Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'コメント内容',
            }),
        }
        labels = {
            'author': '',
            'text': '',
        }


class ReplyForm(ModelForm):
    class Meta:
        model = Reply
        fields = ('author', 'text')
        widgets = {
            'author': TextInput(attrs={
                'class': 'form-control',
                'placeholder': '名前',
            }),
            'text': Textarea(attrs={
                'class': 'form-control',
                'placeholder': '返信内容',
            }),
        }
        labels = {
            'author': '',
            'text': '',
        }

class PhotoForm(forms.Form):
    image = forms.ImageField()

class RestaurantForm(forms.Form):
    name=forms.CharField()
    address=forms.CharField()
    detail=forms.CharField(widget=forms.Textarea)
    image = CloudinaryFileField(options={'folder': 'post_images/', 'tags': 'Restaurant'})

class MenuForm(forms.Form):
    name=forms.CharField(label="メニュー名")
    price=forms.IntegerField(label="料金(税込)")
    description=forms.CharField(label="料理の説明",widget=forms.Textarea)
    image=CloudinaryFileField(options={'folder': 'post_images/', 'tags': 'Menu'})

class CourseForm(forms.Form):
    name=forms.CharField(label="コース名")
    number_of_menus=forms.IntegerField(label="品目数")
    number_of_people=forms.CharField(label="人数")
    price=forms.IntegerField(label="料金(税込)")
    menus=forms.CharField(label="メニュー",widget=forms.Textarea)
    image = CloudinaryFileField(options={'folder': 'post_images/', 'tags': 'course'})
