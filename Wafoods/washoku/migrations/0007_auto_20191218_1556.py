# Generated by Django 2.2.7 on 2019-12-18 06:56

from django.db import migrations
import stdimage.models


class Migration(migrations.Migration):

    dependencies = [
        ('washoku', '0006_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='image',
            field=stdimage.models.StdImageField(blank=True, default=1, upload_to='post_images/'),
            preserve_default=False,
        ),
    ]
