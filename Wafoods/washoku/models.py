from django.db import models
from django.utils import timezone

from cloudinary.models import CloudinaryField
#from stdimage.models import StdImageField

#from imagekit.models import ImageSpecField, ProcessedImageField
#from imagekit.processors import ResizeToFill
# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    tags = models.ManyToManyField(Tag, blank=True)
    title = models.CharField(max_length=255)
    content = models.TextField()
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    published_at = models.DateTimeField(blank=True, null=True)
    is_public = models.BooleanField(default=False)

    image = models.ImageField(
        upload_to='post_images/', null=True, blank=True)

    class Meta:
        ordering = ['-created_at']

    def save(self, *args, **kwargs):
        if self.is_public and not self.published_at:
            self.published_at = timezone.now()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

class ContentImage(models.Model):
    post = models.ForeignKey(Post, on_delete=models.PROTECT)
    content_image = models.ImageField(upload_to='post_content_images/')

class Comment(models.Model):
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(max_length=50)
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=False)

    class Meta:
        ordering = ['-timestamp']

    def approve(self):
        self.approved = True
        self.save()

    def __str__(self):
        return self.text


class Reply(models.Model):
    comment = models.ForeignKey(
        Comment, on_delete=models.CASCADE, related_name='replies')
    author = models.CharField(max_length=50)
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=False)

    def approve(self):
        self.approved = True
        self.save()

    def __str__(self):
        return self.text
###############################

class Restaurant(models.Model):
    name=models.CharField(max_length=256)
    address=models.TextField()
    detail=models.TextField()
    detail_en=models.TextField()
    owner_id=models.IntegerField()

    image = CloudinaryField('image', blank=True, null=True,)
    
    def __str__(self):
        return self.name
    


class Photo(models.Model):
    image = models.ImageField(upload_to='washoku')


class Menu(models.Model):
    name=models.CharField(max_length=256)
    name_en=models.CharField(max_length=256)
    price=models.IntegerField()
    description=models.TextField()
    description_en=models.TextField()
    restaurant_id=models.IntegerField()

    image = CloudinaryField('image', blank=True, null=True,)

    def __str__(self):
        return self.name
    

class Course(models.Model):
    name=models.CharField(max_length=256)
    name_en=models.CharField(max_length=256)
    number_of_menus=models.IntegerField()
    number_of_people=models.CharField(max_length=256)
    price=models.IntegerField()
    menus=models.TextField()
    menus_en=models.TextField()
    restaurant_id=models.IntegerField()

    image = CloudinaryField('image', blank=True, null=True,)

    def __str__(self):
        return self.name
