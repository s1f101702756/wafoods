from django.urls import path
from django.conf.urls import url
from washoku import views
from washoku.views import (
    IndexView,
    PostDetailView,
    CategoryListView,
    CategoryPostView,
    TagListView,
    TagPostView,
    SearchPostView,
    CommentFormView,
    comment_approve,
    comment_remove,
    ReplyFormView,
    reply_approve,
    reply_remove,
)

app_name = 'washoku'
urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post_detail'),
    path('categories/', CategoryListView.as_view(), name='category_list'),
    path('category/<str:category_slug>/',
         CategoryPostView.as_view(), name='category_post'),
    path('tags/', TagListView.as_view(), name='tag_list'),
    path('tag/<str:tag_slug>/', TagPostView.as_view(), name='tag_post'),
    path('search/', SearchPostView.as_view(), name='search_post'),
    path('comment/<int:pk>/', CommentFormView.as_view(), name='comment_form'),
    path('comment/<int:pk>/approve/', comment_approve, name='comment_approve'),
    path('comment/<int:pk>/remove/', comment_remove, name='comment_remove'),
    path('reply/<int:pk>/', ReplyFormView.as_view(), name='reply_form'),
    path('reply/<int:pk>/approve/', reply_approve, name='reply_approve'),
    path('reply/<int:pk>/remove/', reply_remove, name='reply_remove'),

    
    path('create_restaurant/', views.create_restaurant, name='create_restaurant'),
    url(r'restaurant/([0-9]+)/',views.restaurantview,name='restaurant'),
    url(r'restaurant_en/([0-9]+)/',views.restaurantview_en,name='restaurant_en'),
    url(r'restaurant_list/delete/([0-9]+)',views.restaurant_delete,name='restaurant_delete'),
    url(r'restaurant_list/',views.RestaurantListView.as_view(),name='restaurant_list'),
    url(r'reserve_list/([0-9]+)',views.reserve_list,name="reserve_list"),
    
    url(r'calc_menu_en/([0-9]+)',views.calc_menu_en,name='calc_menu_en'),
    url(r'calc_menu/([0-9]+)',views.calc_menu,name='calc_menu'),
    

    url(r'make_menu/([0-9]+)',views.make_menu,name='make_menu'),
    url(r'menu/([0-9]+)/delete/',views.menu_delete,name='menu_delete'), 
    url(r'menu_en/([0-9]+)',views.menu_en,name='menu_en'),  
    url(r'menu/([0-9]+)',views.menu,name='menu'), 
    
    url(r'make_course/([0-9]+)',views.make_course,name='make_course'),
    url(r'course/([0-9]+)/delete/',views.course_delete,name='course_delete'),
    url(r'course_en/([0-9]+)',views.course_en,name='course_en'),  
    url(r'course/([0-9]+)',views.course,name='course'), 


    
    

]