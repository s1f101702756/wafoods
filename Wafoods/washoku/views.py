from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Q
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from washoku.forms import CommentForm, ReplyForm, RestaurantForm, CourseForm, MenuForm
from washoku.models import Post, Category, Tag, Comment, Reply, Restaurant, Menu, Course
from reservation.models import Reserve

from googletrans import Translator


from PIL import Image
# Create your views here.

class PostDetailView(DetailView):
    model = Post

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        if not obj.is_public and not self.request.user.is_authenticated:
            raise Http404
        return obj


class IndexView(ListView):
    model = Restaurant
    template_name = 'washoku/index.html'
    paginate_by = 3


class CategoryPostView(ListView):
    model = Post
    template_name = 'washoku/category_post.html'

    def get_queryset(self):
        category_slug = self.kwargs['category_slug']
        self.category = get_object_or_404(Category, slug=category_slug)
        qs = super().get_queryset().filter(category=self.category)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = self.category
        return context


class TagPostView(ListView):
    model = Post
    template_name = 'washoku/tag_post.html'

    def get_queryset(self):
        tag_slug = self.kwargs['tag_slug']
        self.tag = get_object_or_404(Tag, slug=tag_slug)
        qs = super().get_queryset().filter(tags=self.tag)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tag'] = self.tag
        return context


class SearchPostView(ListView):
    model = Post
    template_name = 'washoku/search_post.html'
    paginate_by = 3

    def get_queryset(self):
        query = self.request.GET.get('q', None)
        lookups = (
            Q(title__icontains=query) |
            Q(content__icontains=query) |
            Q(category__name__icontains=query) |
            Q(tags__name__icontains=query)
        )
        if query is not None:
            qs = super().get_queryset().filter(lookups).distinct()
            return qs
        qs = super().get_queryset()
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        query = self.request.GET.get('q')
        context['query'] = query
        return context


class CategoryListView(ListView):
    queryset = Category.objects.annotate(
        num_posts=Count('post', filter=Q(post__is_public=True)))


class TagListView(ListView):
    queryset = Tag.objects.annotate(num_posts=Count(
        'post', filter=Q(post__is_public=True)))


class CommentFormView(CreateView):
    model = Comment
    form_class = CommentForm

    def form_valid(self, form):
        comment = form.save(commit=False)
        post_pk = self.kwargs['pk']
        comment.post = get_object_or_404(Post, pk=post_pk)
        comment.save()
        return redirect('washoku:post_detail', pk=post_pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post_pk = self.kwargs['pk']
        context['post'] = get_object_or_404(Post, pk=post_pk)
        return context


@login_required
def comment_approve(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('washoku:post_detail', pk=comment.post.pk)


@login_required
def comment_remove(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.delete()
    return redirect('washoku:post_detail', pk=comment.post.pk)


class ReplyFormView(CreateView):
    model = Reply
    form_class = ReplyForm

    def form_valid(self, form):
        reply = form.save(commit=False)
        comment_pk = self.kwargs['pk']
        reply.comment = get_object_or_404(Comment, pk=comment_pk)
        reply.save()
        return redirect('washoku:post_detail', pk=reply.comment.post.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        comment_pk = self.kwargs['pk']
        context['comment'] = get_object_or_404(Comment, pk=comment_pk)
        return context


@login_required
def reply_approve(request, pk):
    reply = get_object_or_404(Reply, pk=pk)
    reply.approve()
    return redirect('washoku:post_detail', pk=reply.comment.post.pk)


@login_required
def reply_remove(request, pk):
    reply = get_object_or_404(Reply, pk=pk)
    reply.delete()
    return redirect('washoku:post_detail', pk=reply.comment.post.pk)


#########

class MenuCourseListView(ListView):
    model = Menu
    context_object_name='menu'
    template_name='washoku/snippets/menu_course_list.html'
    paginate_by = 5
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'courses' : Course.objects.all()
        })
        return context



def restaurantview(request,restaurant_id):
    if request.method == 'GET':
        return render(request, 'washoku/restaurant.html', {
            'menu' : Menu.objects.all(),
            'courses' : Course.objects.all(),
            'restaurant' : Restaurant.objects.get(pk=restaurant_id),
        })

def restaurantview_en(request,restaurant_id):
    if request.method == 'GET':
        return render(request, 'washoku/restaurant_en.html', {
            'menu' : Menu.objects.all(),
            'courses' : Course.objects.all(),
            'restaurant' : Restaurant.objects.get(pk=restaurant_id),
        })

def create_restaurant(request):
    if request.method=="GET":
        form=RestaurantForm()
        return render(request, 'washoku/create_restaurant.html', {'form':form})
    elif request.method=="POST":
        form = RestaurantForm(request.POST, request.FILES)
        if not form.is_valid():
            raise ValueError('invalid form')

        trans=Translator()

        sent_detail_en=trans.translate(form.cleaned_data['detail'],dest="en").text

        restaurant = Restaurant()
        restaurant.name = form.cleaned_data['name']
        restaurant.address = form.cleaned_data['address']
        restaurant.detail = form.cleaned_data['detail']
        restaurant.detail_en = sent_detail_en
        restaurant.owner_id = request.user.id

        restaurant.image = form.cleaned_data['image']

        restaurant.save()
        return redirect("restaurant/"+str(restaurant.id))
    return render(request,'washoku/create_restaurant.html')

def restaurant_delete(request,rest_id):
    restaurant = Restaurant.objects.get(pk=rest_id)
    restaurant.delete()
    return redirect('/')


from .forms import PhotoForm
from .models import Photo


def photo(req):
    if req.method == 'GET':
        return render(req, 'washoku/create_restaurant.html', {
            'form': PhotoForm(),
            'photos': Photo.objects.all(),
        })

    elif req.method == 'POST':                                                     
        form = PhotoForm(req.POST, req.FILES)
        if not form.is_valid():
            raise ValueError('invalid form')

        photo = Photo()
        photo.image = form.cleaned_data['image']
        photo.save()

        return redirect('/')

def make_menu(request,rest_id):
    
    if request.method == 'GET':
        form=MenuForm()
        return render(request, 'washoku/make_menu.html', {'form':form})

    if request.method=="POST":
        form = MenuForm(request.POST, request.FILES)
        if not form.is_valid():
            raise ValueError('invalid form')

        trans=Translator()

        sent_name_en=trans.translate(form.cleaned_data['name'],dest="en").text
        sent_description_en=trans.translate(form.cleaned_data['description'],dest="en").text

        menu=Menu()
        menu.name=form.cleaned_data['name']
        menu.name_en=sent_name_en
        menu.price=form.cleaned_data['price']
        menu.description=form.cleaned_data['description']
        menu.description_en=sent_description_en
        menu.restaurant_id=rest_id

        menu.image = form.cleaned_data['image'] 
        menu.save()
        return redirect('restaurant/'+str(rest_id))

def menu(request,menu_id):
    menu = Menu.objects.get(pk=menu_id)
    data={"menu":menu}
    return render(request,'washoku/menu.html',data)

def menu_en(request,menu_id):
    menu = Menu.objects.get(pk=menu_id)
    data={"menu":menu}
    return render(request,'washoku/menu_en.html',data)

def menu_delete(request,menu_id):
    menu = Menu.objects.get(pk=menu_id)
    rest_id=menu.restaurant_id
    menu.delete()
    return redirect('/restaurant/'+str(rest_id))


def make_course(request,rest_id):
    if request.method == 'GET':
        form=CourseForm()
        return render(request, 'washoku/make_course.html', {'form':form})

    elif request.method == 'POST':                                                     
        form = CourseForm(request.POST, request.FILES)
        if not form.is_valid():
            raise ValueError('invalid form')

        trans=Translator()
        sent_name_en=trans.translate(form.cleaned_data['name'],dest="en").text
        sent_menus_en=trans.translate(form.cleaned_data['menus'],dest="en").text
        

        course = Course()
        course.name=form.cleaned_data['name']
        course.name_en=sent_name_en
        course.number_of_menus=form.cleaned_data['number_of_menus']
        course.number_of_people=form.cleaned_data['number_of_people']
        course.price=form.cleaned_data['price']
        course.menus=form.cleaned_data['menus']
        course.menus_en=sent_menus_en
        course.restaurant_id=rest_id

        course.image = form.cleaned_data['image']

        course.save()

        return redirect('restaurant/'+str(rest_id))

def course(request,course_id):
    course = Course.objects.get(pk=course_id)
    data={"course":course}
    return render(request,'washoku/course.html',data)

def course_en(request,course_id):
    course = Course.objects.get(pk=course_id)
    data={"course":course}
    return render(request,'washoku/course_en.html',data)

def course_delete(request,course_id):
    course = Course.objects.get(pk=course_id)
    rest_id=course.restaurant_id
    course.delete()
    return redirect('/restaurant/'+str(rest_id))

def calc_menu(request,restaurant_id):
    if request.method == 'GET':
        return render(request, 'washoku/calc_menu.html', {
            'menu' : Menu.objects.all(),
            'restaurant' : Restaurant.objects.get(pk=restaurant_id)
        })

def calc_menu_en(request,restaurant_id):
    if request.method == 'GET':
        return render(request, 'washoku/calc_menu_en.html', {
            'menu' : Menu.objects.all(),
            'restaurant' : Restaurant.objects.get(pk=restaurant_id)
        })

class RestaurantListView(ListView):
    model=Restaurant
    template_name="washoku/restaurant_list.html"
    paginate_by=3


def reserve_list(request,restaurant_id):
    """予約一覧"""
    reservation=Reserve.objects.all()
    restaurant=Restaurant.objects.get(pk=restaurant_id)
    return render(request,'washoku/reserve_list.html',{'reservation':reservation,"restaurant":restaurant})