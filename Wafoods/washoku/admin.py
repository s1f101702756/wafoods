from django.contrib import admin
from washoku.models import Category, Tag, Post, ContentImage, Comment, Reply, Restaurant, Menu, Course
# Register your models here.

class ContentImageInline(admin.TabularInline):
    model = ContentImage
    extra = 1


class PostAdmin(admin.ModelAdmin):
    inlines = [
        ContentImageInline,
    ]

admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Reply)
admin.site.register(Restaurant)
admin.site.register(Menu)
admin.site.register(Course)
