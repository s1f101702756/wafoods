from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .forms import ReserveForm
from reservation.models import Reserve

# Create your views here.

def confirm(request,reserve_id):
    reservation = Reserve.objects.get(pk=reserve_id)
    data={"reservation":reservation}
    return render(request,'reservation/confirmation.html',data)

def reserve(request,rest_id):
    if request.method == 'GET':
        form=ReserveForm()
        return render(request,'reservation/reserve.html', {'form':form})

    elif request.method=="POST":
        form = ReserveForm(request.POST, request.FILES)
        if not form.is_valid():
            raise ValueError('invalid form')

        reserve=Reserve()
        reserve.name=form.cleaned_data['name']
        reserve.datetime=form.cleaned_data['datetime']
        reserve.number_of_people=form.cleaned_data['number_of_people']
        reserve.user_id=request.user.id
        reserve.restaurant_id=rest_id
        
        reserve.save()
        return redirect('confirmation/'+str(reserve.id))
    

@login_required
def reserve_delete(request,reserve_id):
    Reserve.objects.get(pk=reserve_id).delete()
    return redirect("my_reserve/")

@login_required
def my_reserve(request):
    """予約一覧"""
    reservation=Reserve.objects.all()
    return render(request,'reservation/my_reserve.html',{'reservation':reservation})