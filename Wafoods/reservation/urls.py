from django.urls import path
from reservation import views
from django.conf.urls import url

app_name="reservation"
urlpatterns = [
    url(r'confirmation/([0-9]+)',views.confirm,name='confirm'),
    url(r'my_reserve/delete/([0-9]+)/$',views.reserve_delete,name='reserve_delete'),
    url(r'my_reserve',views.my_reserve,name='my_reserve'),
    url(r'([0-9]+)',views.reserve,name='reserve'),
]