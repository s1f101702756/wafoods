from django.db import models

# Create your models here.

class Reserve(models.Model):
    name=models.CharField(max_length=256)
    datetime=models.DateTimeField()
    number_of_people=models.IntegerField()
    user_id=models.IntegerField()
    restaurant_id=models.IntegerField()
    def __str__(self):
        return self.name
