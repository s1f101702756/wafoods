from django import forms
from .models import Reserve
from django.utils import timezone

class ReserveForm(forms.Form):
    name=forms.CharField()
    datetime = forms.DateTimeField(
        widget=forms.DateTimeInput(attrs={"type": "datetime-local", "value": timezone.datetime.now().strftime('%Y-%m-%dT%H:%M')}),
        input_formats=['%Y-%m-%dT%H:%M'])
    number_of_people=forms.IntegerField()
    
