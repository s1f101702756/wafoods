from django.shortcuts import render, redirect
from .forms import ModelnameCreateForm
from .models import Model_name

# Create your views here.
def index(request):
    deta={'posts' : Model_name.objects.all()}
    return render(request,"qee/index.html",deta)

import cloudinary

def post(request):
    if request.method=="GET":
        form=ModelnameCreateForm()
        return render(request, 'qee/form.html', {'form':form})
    elif request.method=="POST":
        form = ModelnameCreateForm(request.POST, request.FILES)
        if not form.is_valid():
            raise ValueError('invalid form')

        qee = Model_name()
        qee.image = form.cleaned_data['image']

        qee.save()
        return redirect("/")