from cloudinary.forms import CloudinaryFileField #追加
from .models import Model_name
from django import forms

class ModelnameCreateForm(forms.ModelForm):
    image = CloudinaryFileField(options={'folder': 'media/Model_image', 'tags': 'Model_name'})
    class Meta:
        model = Model_name
        fields = ('image',)
    

